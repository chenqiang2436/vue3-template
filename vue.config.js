const baseURL = require('./src/utils/ipconfig')
const proxyList = {}
const name = 'ME管理平台'
Object.keys(baseURL).forEach(index => {
  proxyList[`/${index}`] = {
    target: baseURL[index],
    changeOrigin: true, // 是否跨域
    pathRewrite: {
      [`^/${index}`]: '' // 重写接口
    }
  }
})
module.exports = {
  devServer: {
    open: true,
    hot: true,
    // before(app){
    //   console.log(app)
    // },
    proxy: proxyList
  },
  configureWebpack: config => {
    config.resolve.extensions = ['.js', '.ts', '.json', '.vue', '.scss', '.css']
  }
}
