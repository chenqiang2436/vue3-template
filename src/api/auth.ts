import {apiSso} from '@/utils/request'
const api = {
  login:{
    method: 'post',
    url:'/sso/login.htm'
  },
  logout:{
    url: '/sso/logout',
    method: 'post',
  },
  getTreeList:{
    url: '/api-ds/addressBook/listAll.htm',
    method: 'post'
  }
}
export default Object.keys(api).reduce((pre:any,serviceName)=>{
  pre[serviceName]=(options)=>apiSso.request({
    ...api[serviceName],
    ...options
  })
  return pre;
},{})




