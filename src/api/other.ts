import {api} from '@/utils/request'


export function getTreeList() {
  return api.request({
    url: '/api-ds/addressBook/listAll.htm',
    method: 'post'
  })
}
