import {getToken, setToken, removeToken} from '@/utils/storage/auth'
import auth from "@/api/auth";
import {mapActions} from "@/store/mapper";

interface actionParam {
    state: any,
    commit: any,
    dispatch: any,
    rootState: any,
    rootGetters: any
}


export default {
    state: {
        token: '',
        userName: ''
    },
    mutations: {
        SET_TOKEN(state: any, token: string) {
            state.token = token
        }
    },
    actions: {
        ...mapActions(['login']),
    }
}
