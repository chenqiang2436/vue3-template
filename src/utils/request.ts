import axios from '@/utils/axios'
const baseURL = require('@/utils/ipconfig')
const $http = Object.keys(baseURL).reduce((cur: any, item) => {
    cur[item] = new axios({baseUrl: process.env.NODE_ENV === 'development' ? `/${item}` : baseURL[item], timeout: 4000})
    return cur
}, {})
export default $http
export const apiSso = $http.Sso
export const api = $http.api
