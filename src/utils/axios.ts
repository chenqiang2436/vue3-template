import axios from 'axios'
import qs from 'qs'
import {message} from 'ant-design-vue'

class AjaxRequest {
    private baseURL: String;
    private timeout: number;
    private queue: Object;
    private loading: any;

    constructor({baseUrl, timeout}) {
        this.baseURL = baseUrl;
        this.timeout = timeout || 3000;
        this.queue = {};
    }

    merge(options) {
        return {...options, baseURL: this.baseURL, timeout: this.timeout};
    }

    deleteQueue(url) {
        delete this.queue[url];
        if (Object.keys(this.queue).length === 0) {
            this.loading ? this.loading() : void 0;
        }
    }

    setInterceptor(instance, url) {
        instance.interceptors.request.use(config => {
            if (Object.keys(this.queue).length === 0) {
                // const hide = message.loading('Action in progress..', 0);
                message.config({
                    top: `200px`,
                    duration: 2,
                    maxCount: 3,
                });
                this.loading = message.loading('加载中', 0);
            }
            if (!/^application\/json/gi.test(config.headers['Content-Type']) && config.data && Object.prototype.toString.call(config.data).toLowerCase() !== '[object formdata]') {
                config.data = qs.stringify({
                    ...config.data
                })
            }
            this.queue[url] = url;
            return config;
        });
        // 响应拦截

        // noinspection JSCheckFunctionSignatures
        instance.interceptors.response.use(
            response => {
                this.deleteQueue(url);
                const status = response.status
                const res = response.data
                if (status == 200 || status == 201 || status == 204) {
                    return response.data
                } else {
                    return Promise.reject(res)
                }
            },
            error => {
                this.deleteQueue(url);
                throw error;
            }
        );
    }

    request(options) {
        let instance = axios.create();
        this.setInterceptor(instance, options.url);
        let config = this.merge(options);
        return instance(config);
    }
}
export default AjaxRequest
