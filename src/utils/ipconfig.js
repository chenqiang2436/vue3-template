const urlData = {
    production: {
        api: 'https://www.huaitaos.com/prod',
        Sso: 'https://www.huaitaos.com/prod-login',
    },
    staging:{
        api: 'https://www.huaitaos.com/prod',
        Sso: 'https://www.huaitaos.com/stage-login'
    },
    development:{
        api: 'https://www.huaitaos.com/stage',
        Sso: 'https://www.huaitaos.com/stage-login',
    }

}
module.exports = urlData[process.env.VUE_APP_ENV]
