const Cookies = require('js-cookie')

const TokenKey = 'userCode'

// 用户登录token
export const getToken = () => {
  return Cookies.get(TokenKey)
}

export const setToken = (token:string) => {
  Cookies.set(TokenKey, token)
}

export const removeToken = () => {
  Cookies.remove(TokenKey)
}
